/*
This file is part of the Very Simple Data Structures Library (VSDSL)
Copyright (C) 2021 Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "common.h"

int to_int(vsdsl_pointer pointer)
{
    return *(int *)pointer;
}

float to_float(vsdsl_pointer pointer)
{
    return *(float *)pointer;
}

double to_double(vsdsl_pointer pointer)
{
    return *(double *)pointer;
}

char to_char(vsdsl_pointer pointer)
{
    return *(char *)pointer;
}

short to_short(vsdsl_pointer pointer)
{
    return *(short *)pointer;
}

long to_long(vsdsl_pointer pointer)
{
    return *(long *)pointer;
}
