/*
This file is part of the Very Simple Data Structures Library (VSDSL)
Copyright (C) 2021 Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "queue.h"

/*
 * vsdsl_queue_init
 * Parameters: pointer to queue
 * Returns: void
 * Initializes the queue
*/
void vsdsl_queue_init(Queue *queue)
{
    queue->first = queue->last = NULL;
    queue->size = 0;
}

/*
 * vsdsl_queue_enqueue
 * Parameters: pointer to queue, pointer to value
 * Returns: vsdsl_status (VSDSL_TRUE or VSDSL_FALSE)
 * Adds a new value to the end of the queue
*/
vsdsl_status vsdsl_queue_enqueue(Queue *queue, vsdsl_pointer value)
{
    QueueItem *aux;
    aux = malloc(sizeof (QueueItem));

    if(aux != NULL)
    {
        aux->value = value;
        aux->next = NULL;
        if(queue->first == NULL)
        {
            queue->first = aux;
        }
        else
        {
            queue->last->next = aux;
        }
        queue->last = aux;
        queue->size++;
        return VSDSL_TRUE;
    }
    return VSDSL_FALSE;
}

/*
 * vsdsl_queue_getfirst
 * Parameters: pointer to queue
 * Returns: sdsl_pointer (pointer to value)
 * Get the first value (using sdsl_pointer) without removing it
*/
vsdsl_pointer vsdsl_queue_getfirst(Queue *queue)
{
    if(queue->first == NULL) return NULL;
    return queue->first->value;
}

/*
 * vsdsl_queue_dequeue
 * Parameters: pointer to queue
 * Returns: sdsl_return (struct containing a vsdsl_status control variable and vsdsl_pointer value)
 * Get the first value and remove it
*/
vsdsl_return vsdsl_queue_dequeue(Queue *queue)
{
    vsdsl_return result;
    result.pointer = NULL;
    result.status = VSDSL_FALSE;

    if(queue->first != NULL)
    {
        QueueItem *aux;
        aux = queue->first;
        result.pointer = aux->value;
        queue->first = aux->next;
        if(queue->first == NULL) queue->last = NULL;
        free(aux);
        aux = NULL;
        queue->size--;
        result.status = VSDSL_TRUE;
    }
    return result;
}

/*
 * vsdsl_queue_size
 * Parameters: pointer to queue
 * Returns: int
 * Returns the number of items in the given queue as integer
*/
int vsdsl_queue_size(Queue *queue)
{
    return queue->size;
}
