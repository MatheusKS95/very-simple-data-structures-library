/*
This file is part of the Very Simple Data Structures Library (VSDSL)
Copyright (C) 2021 Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "list.h"

/*
 * vsdsl_list_init
 * Parameters: pointer to list
 * Returns: void
 * Initializes the list
*/
void vsdsl_list_init(List *list, int list_limiter)
{
    list->first = list->last = NULL;
    list->size = 0;
    list->limiter = list_limiter;
}

/*
 * vsdsl_list_add
 * Parameters: pointer to list, pointer to value
 * Returns: vsdsl_status (VSDSL_TRUE or VSDSL_FALSE)
 * Add new value to the end of the list.
*/
vsdsl_status vsdsl_list_add(List *list, vsdsl_pointer value)
{
    ListItem *aux;
    aux = NULL;

    if(list->limiter > 0 && list->size == list->limiter) return VSDSL_FALSE;

    aux = (ListItem *)malloc(sizeof(ListItem));
    if(aux != NULL)
    {
        aux->value = value;
        aux->next = NULL;
        if(list->first == NULL)
        {
            aux->prev = NULL;
            list->first = aux;
        }
        else
        {
            aux->prev = list->last;
            list->last->next = aux;
        }
        list->last = aux;
        list->size++;
        return VSDSL_TRUE;
    }
    return VSDSL_FALSE;
}

/*
 * vsdsl_list_remove
 * Parameters: pointer to list, pointer to value
 * Returns: vsdsl_status (VSDSL_TRUE or VSDSL_FALSE)
 * Remove the value from list.
*/
vsdsl_status vsdsl_list_remove(List *list, vsdsl_pointer value)
{
    ListItem *aux, *behind;
    aux = behind = NULL;
    if(list->first != NULL)
    {
        aux = list->first;
        behind = NULL;
        while(aux != NULL)
        {
            if(aux->value == value) break;
            behind = aux;
            aux = aux->next;
        }

        if(aux == NULL)
        {
            return VSDSL_FALSE;
        }

        if(list->first == list->last)
        {
            list->first = list->last = NULL;
        }
        else
        {
            if(aux == list->first)
            {
                list->first = list->first->next;
            }
            else if (aux == list->last)
            {
                list->last = behind;
                list->last->next = NULL;
            }
            else
            {
                behind->next = aux->next;
            }
        }

        free(aux);
        aux = NULL;
        list->size--;
        return VSDSL_TRUE;
    }
    return VSDSL_FALSE;
}

/*
 * vsdsl_list_export
 * Parameters: pointer to list, char array with file name, data type
 * Returns: vsdsl_status (VSDSL_TRUE or VSDSL_FALSE)
 * Export the list from memory to a text file (values separated by semicolons).
*/
vsdsl_status vsdsl_list_export(List *list, char *filename, vsdsl_type type)
{
    FILE *fp = fopen(filename, "w");
    if(fp == NULL) return VSDSL_FALSE;
    ListItem *aux;
    aux = NULL;
    if(list->first == NULL)
    {
        fprintf(fp, "EMPTY LIST");
    }
    else
    {
        aux = list->first;
        while(aux != NULL)
        {
            switch(type)
            {
                case VSDSL_CHAR: fprintf(fp, "%c;", to_char(aux->value)); break;
                case VSDSL_INT: fprintf(fp, "%d;", to_int(aux->value)); break;
                case VSDSL_FLOAT: fprintf(fp, "%f;", (double)to_float(aux->value)); break;
                case VSDSL_DOUBLE: fprintf(fp, "%lf;", to_double(aux->value)); break;
                case VSDSL_LONG: fprintf(fp, "%li;", to_long(aux->value)); break;
                case VSDSL_SHORT: fprintf(fp, "%hi;", to_short(aux->value)); break;
                default: break;
            }
            aux = aux->next;
        }
    }
    fclose(fp);
    return VSDSL_TRUE;
}

/*
 * vsdsl_list_import
 * Parameters: pointer to list, char array with file name, data type
 * Returns: vsdsl_status (VSDSL_TRUE or VSDSL_FALSE)
 * Import the list from text file to memory.
*/
vsdsl_status vsdsl_list_import(List *list, char *filename, vsdsl_type type)
{
    FILE *fp = fopen(filename, "r");
    vsdsl_status status = VSDSL_TRUE;

    if(fp == NULL)
    {
        return VSDSL_FALSE;
    }

    if(type == VSDSL_INT)
    {
        int aux;
        while(fscanf(fp, "%d;", &aux) > 0)
        {
            if(vsdsl_list_add(list, &aux) == VSDSL_FALSE)
            {
                status = VSDSL_FALSE;
            }
        }
    }
    else if(type == VSDSL_CHAR)
    {
        char aux;
        while(fscanf(fp, "%c;", &aux) > 0)
        {
            if(vsdsl_list_add(list, &aux) == VSDSL_FALSE)
            {
                status = VSDSL_FALSE;
            }
        }
    }
    else if(type == VSDSL_LONG)
    {
        long aux;
        while(fscanf(fp, "%li;", &aux) > 0)
        {
            if(vsdsl_list_add(list, &aux) == VSDSL_FALSE)
            {
                status = VSDSL_FALSE;
            }
        }
    }
    else if(type == VSDSL_FLOAT)
    {
        float aux;
        while(fscanf(fp, "%f;", &aux) > 0)
        {
            if(vsdsl_list_add(list, &aux) == VSDSL_FALSE)
            {
                status = VSDSL_FALSE;
            }
        }
    }
    else if(type == VSDSL_SHORT)
    {
        short aux;
        while(fscanf(fp, "%hi;", &aux) > 0)
        {
            if(vsdsl_list_add(list, &aux) == VSDSL_FALSE)
            {
                status = VSDSL_FALSE;
            }
        }
    }
    else if(type == VSDSL_DOUBLE)
    {
        double aux;
        while(fscanf(fp, "%lf;", &aux) > 0)
        {
            if(vsdsl_list_add(list, &aux) == VSDSL_FALSE)
            {
                status = VSDSL_FALSE;
            }
        }
    }
    else
    {
        status = VSDSL_FALSE;
    }

    fclose(fp);
    return status;
}

/*
 * vsdsl_sequential_search
 * Parameters: pointer to list, a pointer to a value to be compared and it's type
 * Returns: vsdsl_pointer (to value in list)
 * Search (sequentially) for a value stored in list.
*/
vsdsl_pointer vsdsl_sequential_search(List *list, vsdsl_pointer value, vsdsl_type type)
{
    ListItem *aux;
    aux = list->first;

    while(aux != NULL)
    {
        switch(type)
        {
            case VSDSL_INT: if(to_int(value) == to_int(aux->value)) return aux->value; break;
            case VSDSL_CHAR: if(to_char(value) == to_char(aux->value)) return aux->value; break;
            case VSDSL_FLOAT: if(to_float(value) - to_float(aux->value) < EPSILON) return aux->value; break;
            case VSDSL_DOUBLE: if(to_double(value) - to_double(aux->value) < (double)EPSILON) return aux->value; break;
            case VSDSL_LONG: if(to_long(value) == to_long(aux->value)) return aux->value; break;
            case VSDSL_SHORT: if(to_short(value) == to_short(aux->value)) return aux->value; break;
            default: break;
        }

        aux = aux->next;
    }
    return NULL;
}
