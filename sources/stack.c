/*
This file is part of the Very Simple Data Structures Library (VSDSL)
Copyright (C) 2021 Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stack.h"

/*
 * vsdsl_stack_init
 * Parameters: pointer to stack
 * Returns: void
 * Initializes the stack
*/
void vsdsl_stack_init(Stack *stack)
{
    stack->top = NULL;
    stack->size = 0;
}

/*
 * vsdsl_stack_push
 * Parameters: pointer to stack, pointer to value
 * Returns: vsdsl_status (VSDSL_TRUE or VSDSL_FALSE)
 * Adds a new value to the top of the stack
*/
vsdsl_status vsdsl_stack_push(Stack *stack, vsdsl_pointer value)
{
    StackItem *aux;
    aux = malloc(sizeof(StackItem));

    if(aux != NULL)
    {
        aux->value = value;
        aux->next = stack->top;
        stack->top = aux;
        stack->size++;
        return VSDSL_TRUE;
    }

    return VSDSL_FALSE;
}

/*
 * vsdsl_stack_pop
 * Parameters: pointer to stack
 * Returns: sdsl_return (struct containing a vsdsl_status control variable and vsdsl_pointer value)
 * Get the top value and remove it
*/
vsdsl_return vsdsl_stack_pop(Stack *stack)
{
    vsdsl_return content;
    content.pointer = NULL;
    StackItem *aux = stack->top;
    if(aux == NULL)
    {
        content.status = VSDSL_FALSE;
        return content;
    }

    content.pointer = aux->value;
    content.status = VSDSL_TRUE;
    stack->top = stack->top->next;
    stack->size--;

    free(aux);
    aux = NULL;

    return content;
}

/*
 * vsdsl_stack_gettop
 * Parameters: pointer to stack
 * Returns: vsdsl_pointer (pointer to value)
 * Get the top value (using vsdsl_pointer) without removing it
*/
vsdsl_pointer vsdsl_stack_gettop(Stack *stack)
{
    if(stack->top == NULL) return NULL;
    return stack->top->value;
}

/*
 * vsdsl_stack_size
 * Parameters: pointer to stack
 * Returns: int
 * Returns the number of items in the given stack as integer
*/
int vsdsl_stack_size(struct Stack *stack)
{
    return stack->size;
}
