/*
This file is part of the Very Simple Data Structures Library (VSDSL)
Copyright (C) 2021 Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VSDSL_STACK_H
#define VSDSL_STACK_H

#include "common.h"

typedef struct StackItem
{
    vsdsl_pointer value;
    struct StackItem *next;
} StackItem;

typedef struct Stack
{
    int size;
    StackItem *top;
} Stack;

void vsdsl_stack_init(Stack *stack);
vsdsl_status vsdsl_stack_push(Stack *stack, vsdsl_pointer value);
vsdsl_return vsdsl_stack_pop(Stack *stack);
vsdsl_pointer vsdsl_stack_gettop(Stack *stack);
int vsdsl_stack_size(Stack *stack);

#endif
