/*
This file is part of the Very Simple Data Structures Library (VSDSL)
Copyright (C) 2021 Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VSDSL_COMMON_H
#define VSDSL_COMMON_H

#include <stdio.h>
#include <stdlib.h>

#define EPSILON 0.001f

//Pointer to value being used (C doesn't have templates like C++)
typedef void* vsdsl_pointer;

//Used for boolean-like returns
typedef enum vsdsl_status
{
    VSDSL_TRUE,
    VSDSL_FALSE
} vsdsl_status;

//Used for export to file function in lists
typedef enum vsdsl_type
{
    VSDSL_INT,
    VSDSL_FLOAT,
    VSDSL_DOUBLE,
    VSDSL_SHORT,
    VSDSL_LONG,
    VSDSL_CHAR
} vsdsl_type;

//Used when two returns are requested (mostly on functions that removes values)
typedef struct vsdsl_return
{
    vsdsl_status status;
    vsdsl_pointer pointer;
} vsdsl_return;

int to_int(vsdsl_pointer pointer);
float to_float(vsdsl_pointer pointer);
double to_double(vsdsl_pointer pointer);
char to_char(vsdsl_pointer pointer);
short to_short(vsdsl_pointer pointer);
long to_long(vsdsl_pointer pointer);

#endif
