/*
This file is part of the Very Simple Data Structures Library (VSDSL)
Copyright (C) 2021 Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VSDSL_QUEUE_H
#define VSDSL_QUEUE_H

#include "common.h"

typedef struct QueueItem
{
    vsdsl_pointer value;
    struct QueueItem *next;
} QueueItem;

typedef struct Queue
{
    int size;
    QueueItem *first;
    QueueItem *last;
} Queue;

void vsdsl_queue_init(Queue *queue);
vsdsl_status vsdsl_queue_enqueue(Queue *queue, vsdsl_pointer value);
vsdsl_pointer vsdsl_queue_getfirst(Queue *queue);
vsdsl_return vsdsl_queue_dequeue(Queue *queue);
int vsdsl_queue_size(Queue *queue);

#endif
