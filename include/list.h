/*
This file is part of the Very Simple Data Structures Library (VSDSL)
Copyright (C) 2021 Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VSDSL_LIST_H
#define VSDSL_LIST_H

#include "common.h"

typedef struct ListItem
{
    struct ListItem *prev;
    vsdsl_pointer value;
    struct ListItem *next;
} ListItem;

typedef struct List
{
    ListItem *first;
    ListItem *last;
    int size;
    int limiter;
} List;

void vsdsl_list_init(List *list, int list_limiter);
vsdsl_status vsdsl_list_add(List *list, vsdsl_pointer value);
vsdsl_status vsdsl_list_remove(List *list, vsdsl_pointer value);
vsdsl_status vsdsl_list_export(List *list, char *filename, vsdsl_type type);
vsdsl_status vsdsl_list_import(List *list, char *filename, vsdsl_type type);
vsdsl_pointer vsdsl_sequential_search(List *list, vsdsl_pointer value, vsdsl_type type);

#endif
