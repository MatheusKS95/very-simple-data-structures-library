# Very Simple Data Structures Library (VSDSL) #

VSDSL is a simple WIP data structure C library. Licenced under GNU LGPL.

### What is this repository for? ###

This is the repository of a data structure library with support for stacks, queues and doubly-linked lists. Any content can be added in these structures (however, for anything value-related such as comparisons VSDSL supports int, float, double, char, long and short at the moment).

This is a working-in-process project, only the barebones were coded, no formal testing was made and there are no version tags nor release yet.

### How do I get set up? ###

Build the project using CMake (version 2.9). I recommend CMake GUI for ease of use. It'll build as a shared library (.so in Unix-like systems and .dll on Windows).

After that it depends on what platform you chose. You can either install it on your system (Unix-like) or simply put into your project and link it. If needed, you also have to copy the contents from include directory.

It's licenced under GNU LGPL v3. That means you can use in your closed-source projects if you dynamically link it (default build). Anyways, follow what LGPL tells what you may or may not do.

### Contribution guidelines ###

There are a lot of things to do.

### Who do I talk to? ###

Repo owner: Matheus.